<?php
// $Id: uc_minpercentrate.admin.inc,v 1.4 2009/07/05 04:59:20 khalemi Exp $

/**
 * @file
 * Minimum rate shipping method administration menu items.
 *
 */

/**
 * List and compare all minpercentrate shipping quote methods.
 */
function uc_minpercentrate_admin_methods() {
  $output = '';

  $rows = array();
  $enabled = variable_get('uc_quote_enabled', array());
  $weight = variable_get('uc_quote_method_weight', array());
  $result = db_query("SELECT mid, title, label, base_rate, percent_rate, free_rate FROM {uc_minpercentrate_methods}");
  while ($method = db_fetch_object($result)) {
    $row = array();
    $row[] = check_plain($method->title);
    $row[] = check_plain($method->label);
    $row[] = uc_currency_format($method->base_rate);
    $row[] = $method->percent_rate . '%';
    $row[] = uc_currency_format($method->free_rate);
    $row[] = l(t('edit'), 'admin/store/settings/quotes/methods/minpercentrate/'. $method->mid);
    $row[] = l(t('conditions'), CA_UI_PATH .'/uc_minpercentrate_get_quote_'. $method->mid .'/edit/conditions');
    $rows[] = $row;
  }
  if (count($rows)) {
    $header = array(t('Title'), t('Label'), t('Base rate'), t('Default product rate'), t('Free rate above'), array('data' => t('Operations'), 'colspan' => 2));
    $output .= theme('table', $header, $rows);
  }
  $output .= l(t('Add a new minimum rate shipping method.'), 'admin/store/settings/quotes/methods/minpercentrate/add');
  return $output;
}

/**
 * Configure the store default product shipping rates.
 */
function uc_minpercentrate_admin_method_edit_form($form_state, $mid = 0) {
  $form = array();
  $sign_flag = variable_get('uc_sign_after_amount', FALSE);
  $currency_sign = variable_get('uc_currency_sign', '$');

  if (is_numeric($mid) && ($method = db_fetch_object(db_query("SELECT * FROM {uc_minpercentrate_methods} WHERE mid = %d", $mid)))) {
    $form['mid'] = array('#type' => 'value', '#value' => $mid);
  }
  $form['title'] = array('#type' => 'textfield',
    '#title' => t('Shipping method title'),
    '#description' => t('The name shown to distinguish it from other minpercentrate methods.'),
    '#default_value' => $method->title,
  );
  $form['label'] = array('#type' => 'textfield',
    '#title' => t('Line item label'),
    '#description' => t('The name shown to the customer when they choose a shipping method at checkout.'),
    '#default_value' => $method->label,
  );
  $form['base_rate'] = array('#type' => 'textfield',
    '#title' => t('Minimum rate'),
    '#description' => t('The starting price to apply the rate.'),
    '#default_value' => $method->base_rate,
    '#size' => 16,
    '#field_prefix' => $sign_flag ? '' : $currency_sign,
    '#field_suffix' => $sign_flag ? $currency_sign : '',
  );
  $form['percent_rate'] = array('#type' => 'textfield',
    '#title' => t('Default product percent shipping rate'),
    '#default_value' => $method->percent_rate,
    '#size' => 16,
    '#field_suffix' => '%',
  );
  $form['free_rate'] = array('#type' => 'textfield',
    '#title' => t('Free rate above'),
    '#default_value' => $method->free_rate,
    '#size' => 16,
    '#field_prefix' => $sign_flag ? '' : $currency_sign,
    '#field_suffix' => $sign_flag ? $currency_sign : '',
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['buttons']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#submit' => array('uc_minpercentrate_admin_method_edit_form_delete'),
  );

  return $form;
}

function uc_minpercentrate_admin_method_edit_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Submit')) {
    if (!empty($form_state['values']['base_rate']) && !is_numeric($form_state['values']['base_rate'])) {
      form_set_error('base_rate', t('The base rate must be a numeric amount.'));
    }
    if (!empty($form_state['values']['percent_rate']) && !is_numeric($form_state['values']['percent_rate'])) {
      form_set_error('percent_rate', t('The product rate must be a numeric amount.'));
    }
    if (!empty($form_state['values']['free_rate']) && !is_numeric($form_state['values']['free_rate'])) {
      form_set_error('free_rate', t('The product free rate must be a numeric amount.'));
    }
  }
}

function uc_minpercentrate_admin_method_edit_form_delete($form, &$form_state) {
  drupal_goto('admin/store/settings/quotes/minpercentrate/'. $form_state['values']['mid'] .'/delete');
}

function uc_minpercentrate_admin_method_edit_form_submit($form, &$form_state) {
  if ($form_state['values']['mid']) {
    db_query("UPDATE {uc_minpercentrate_methods} SET title = '%s', label = '%s', base_rate = %f, percent_rate = %f, free_rate = %f WHERE mid = %d",
      $form_state['values']['title'], $form_state['values']['label'], $form_state['values']['base_rate'], $form_state['values']['percent_rate'], $form_state['values']['free_rate'], $form_state['values']['mid']);
    drupal_set_message(t("Minimum rate shipping method was updated."));
  }
  else {
    db_query("INSERT INTO {uc_minpercentrate_methods} (title, label, base_rate, percent_rate, free_rate) VALUES ('%s', '%s', %f, %f, %f)",
      $form_state['values']['title'], $form_state['values']['label'], $form_state['values']['base_rate'], $form_state['values']['percent_rate'], $form_state['values']['free_rate']);
    $mid = db_last_insert_id('uc_minpercentrate_methods', 'mid');
    $enabled = variable_get('uc_quote_enabled', array());
    $enabled['minpercentrate_'. $mid] = TRUE;
    variable_set('uc_quote_enabled', $enabled);
    $weight = variable_get('uc_quote_method_weight', array());
    $weight['minpercentrate_'. $mid] = 0;
    variable_set('uc_quote_method_weight', $weight);
    drupal_set_message(t("Created and enabled new minimum rate shipping method."));
  }
  $form_state['redirect'] = 'admin/store/settings/quotes/methods/minpercentrate';
}

/******************************************************************************
 * Menu Callbacks                                                             *
 ******************************************************************************/

function uc_minpercentrate_admin_method_confirm_delete($form_state, $mid) {
  $form = array();
  $form['mid'] = array('#type' => 'value', '#value' => $mid);

  return confirm_form($form, t('Do you want to delete this shipping method?'),
    'admin/store/settings/quotes/methods/minpercentrate',
    t('This will remove the shipping method, Conditional Action predicate, and the
      product-specific overrides (if applicable). This action can not be undone.'),
    t('Delete'));
}

function uc_minpercentrate_admin_method_confirm_delete_submit($form, &$form_state) {
  db_query("DELETE FROM {uc_minpercentrate_methods} WHERE mid = %d", $form_state['values']['mid']);
  db_query("DELETE FROM {uc_minpercentrate_products} WHERE mid = %d", $form_state['values']['mid']);
  ca_delete_predicate('uc_minpercentrate_get_quote_'. $form_state['values']['mid']);

  $enabled = variable_get('uc_quote_enabled', array());
  unset($enabled['minpercentrate_'. $form_state['values']['mid']]);
  variable_set('uc_quote_enabled', $enabled);

  $weight = variable_get('uc_quote_method_weight', array());
  unset($weight['minpercentrate_'. $form_state['values']['mid']]);
  variable_set('uc_quote_method_weight', $weight);

  drupal_set_message(t('Minimum rate shipping method deleted.'));
  $form_state['redirect'] = 'admin/store/settings/quotes/methods/minpercentrate';
}

